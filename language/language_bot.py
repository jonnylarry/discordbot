import asyncio
from music.musicbot import text_to_speech
from discord.ext import commands, tasks
from discord import ChannelType, utils
import discord
import config
from datetime import datetime, timedelta
#from language.sprichdeutsch import isAngelsachse

class LanguageBot(commands.Cog):
    def __init__(self, bot):
        self.client = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return
        if "fix" in message.content.lower().strip() and "bot" in message.content.lower().strip():
            await message.delete()
        #elif not is_command(message) and isAngelsachse(message.content):
        #    await message.reply("Sprich deutsch du ...!")

    @commands.command(name='config')
    async def configuration(self, ctx, path, value=None):
        if value is None:
            config.remove(path.split("."))
            await ctx.message.reply("Removed.")
        else:
            config.write_config(path.split("."), value)
            await ctx.message.reply("Set.")
    
    @commands.command(name='torstifizieren')
    async def torstifizieren(self, ctx):
        id = 200715099323891716
        member = await ctx.guild.fetch_member(id)
        #print(member)
        await member.edit(nick="Thorsten")
        await ctx.message.reply("torstifiziert.")

    @commands.command(name='nonameisierung')
    async def nonameisierung(self, ctx):
        id = 261204639838371853
        member = await ctx.guild.fetch_member(id)
        #print(member)
        await member.edit(nick="Mister NoName")
        await ctx.message.reply("nonameisiert.")
    
    @commands.command(name='typisierung')
    async def typisierung(self, ctx):
        id = 261204639838371853
        member = await ctx.guild.fetch_member(id)

        await member.edit(nick="Mister y no command")
        await ctx.message.reply("y no command.")


def is_command(msg): # Checking if the message is a command call
  if len(msg.content) == 0:
      return False
  elif msg.content.startswith('?'):
      return True
  else:
      return False