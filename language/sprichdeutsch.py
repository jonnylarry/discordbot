import enchant
import re


print(enchant.list_languages())
d_angelsachse = enchant.Dict("en_US")
#d_deutsch = enchant.Dict("de_DE")
#d_angelsachse.suggest("Helo")


def isAngelsachse(sentence):
    sentence = re.sub(r'".*"', '', sentence)
    words = re.findall(r"[\w']+", sentence)
    hits = 0
    for w in words:
        if d_angelsachse.check(w):
            if not d_deutsch.check(w):
                hits += 1
    
    THRESHOLD = 0.50 # %
    return hits/len(words) > THRESHOLD

def example():
    sentence = "is this the yellow from the egg?"
    #sentence = "hallo ich habe nix zu befürchten"
    print(isAngelsachse(sentence))
