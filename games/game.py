import string
import random
from discord.ext import commands
import asyncio
import discord


def string_list(header, list):
    return "** " + header + " **\n - " + "\n - ".join(list)

class Message():
    def __init__(self, msg, err_code):
        self.msg = msg
        self.err_code = err_code
    
    async def submit(self, channel):
        await channel.send(self.msg)

    def hasErr(self):
        return self.err_code != 0

class Game:
    def __init__(self, code, client, cog):
        self.name = "Abstract Game"
        self.players = dict()
        self.player_order = []
        self.code = code
        self.started = False
        self.client = client
        self.cog = cog

    def join(self, user):
        if user in self.players:
            return Message(user.name + " schon im spiel. " + self.summary(), 0)
        if self.started:
            return Message("Game already started. " + self.summary(), 1)
        self.players[user] = None
        return Message(user.name + " spielt mit. " + self.summary(), 0)
    
    def leave(self, user):
        if user in self.players:
            self.players.pop(user)
            return Message(user.name + " ist gegangen. " + self.summary(), 0)
        return Message(user.name + " war nicht in Lobby. " + self.summary(), 1)

    def internal_loop(self):
        pass

    def start_conditions(self):
        return True

    async def round(self, channel):
        pass

    async def start(self, channel):
        print("#STARING GAME")
        if self.started:
            return Message("Already started. " + self.summary(), 1)
        if self.start_conditions():
            print("#START CONDITIONS MET")
            self.started = True
            self.player_order = list(self.players.keys())
            random.shuffle(self.player_order)
            self.channels = dict()
            for p in self.player_order:
                self.channels[p] = await p.create_dm()
            return Message("Game started. " + self.summary(), 0)
        else:
            return Message("Start Bedingungen nicht erfüllt. " + self.summary(), 1)

    def playerlist(self):
        return Message(string_list("Players", [u.name for u in self.players.keys()]), 0)

    def summary(self):
        return self.name + " [" + self.code + "]"

    def end(self):
        self.cog.stop_internal(self.code)

    async def wait_for_dm_answers(self, writers, timeout=120):
        print("WAITING FOR DMs FROM " + ", ".join([w.name for w in writers]))
        results = dict()
        authors_left = list(writers)
        while len(authors_left) > 0:
            try:
                def check(msg):
                    return msg.author in authors_left and isinstance(msg.channel, discord.channel.DMChannel)
                msg = await self.client.wait_for("message", check=check, timeout=timeout) # 120 seconds to reply
                authors_left.remove(msg.author)
                results[msg.author] = msg.content
                #results[self.reciever[msg.author]] = msg.content
                print(msg.author.name + " answered")
            except asyncio.TimeoutError:
                return None
        return results









from games.whoami import WhoAmI
from games.nobodyIsPerfect import NobodyIsPerfect
games = {"Game": Game, "WhoAmI": WhoAmI, "NobodyIsPerfect": NobodyIsPerfect}

class Games(commands.Cog):
    def __init__(self, bot):
        self.client = bot
        self.active_games = dict()
        self.global_players = dict()

    @commands.command(name='game.play')
    async def play(self, ctx, name):
        """Starts a new Game of type a. Possible types: Game (Abstract Debug Game), WhoAmI (Wer bin Ich),"""
        message = ctx.message
        code = self.gen_code()
        if name in games:
            self.active_games[code] = games[name](code, self.client, self)
            await message.channel.send(name + " Spielcode: " + code)
        else:
            await message.channel.send(name + " nicht gefunden")

    @commands.command(name='game.join')  
    async def join(self, ctx, code):
        """Joins game a, where a is a code like "ICLM"."""
        message = ctx.message
        user = message.author
        if user in self.global_players:
            await message.channel.send(user.name + " ist schon in einem Spiel.")
            return
        if code in self.active_games:
            m = self.active_games[code].join(user)
            if not m.hasErr():
                self.global_players[user] = self.active_games[code]
            await m.submit(message.channel)
        else:
            await message.channel.send(code + " nicht gefunden")

    @commands.command(name='game.leave')  
    async def leave(self, ctx):
        """Leaves the current game."""
        message = ctx.message
        user = message.author
        if user in self.global_players:
            msg = self.global_players[user].leave(user)
            if not msg.hasErr():
                self.global_players.pop(user)
            await msg.submit(message.channel)

    def stop_internal(self, code=None, user=None):
        game = self.get_game(code, user)
        if game is None:
                return
        for p in game.players:
            self.global_players.pop(p)
        self.active_games.pop(game.code)

    @commands.command(name='game.stop')  
    async def stop(self, ctx, code=None):
        """Stops [my] game or game corresponding to given code a."""
        message = ctx.message
        user = message.author
        
        self.stop_internal(code, user)

    @commands.command(name='game.start')  
    async def start(self, ctx):
        """Starts [my] game."""
        message = ctx.message
        user = message.author
        game = self.get_game(None, user)
        channel = message.channel
        if game is None:
            return
        await game.start(channel)

    @commands.command(name='game.continue')  
    async def continuegame(self, ctx, code=None):
        """Continue [my] game."""
        message = ctx.message
        user = message.author
        game = self.get_game(code, user)
        if game.started:
            await game.round(message.channel)


    @commands.command(name='game.players')  
    async def listplayers(self, ctx, code=None):
        """Lists [my] games players or the players of a given code a."""
        message = ctx.message
        user = message.author
        game = self.get_game(code, user)
        if game is None:
            return
        await game.playerlist().submit(message.channel)

    @commands.command(name='game.games')  
    async def listgames(self, ctx):
        """Lists all active games."""
        message = ctx.message
        print(self.active_games.values())
        await message.channel.send(string_list("Active Games", [g.summary() for g in self.active_games.values()]))


    def get_game(self, code, user):
        if code is not None:
            return self.active_games[code]
        elif user in self.global_players:
            return self.global_players[user]
        else:
            return None
    
    def gen_code(self):
        while True:
            code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=4))
            if code not in self.active_games:
                return code