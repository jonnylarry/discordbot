from games.game import Game, Message, string_list
import asyncio
import discord


class WhoAmI(Game):
    def __init__(self, code, client, cog):
        super().__init__(code, client, cog)
        self.name = "WhoAmI"

    def start_conditions(self):#TODO
        return len(self.players) > 0 and super().start_conditions()

    async def start(self, channel):
        msg = await super().start(channel)
        if msg.hasErr():
            return msg
        else:
            msg.submit(channel)
        n = len(self.players)

        self.writer = dict()
        self.reciever = dict()
        for i in range(1, n):
            w = self.player_order[i-1]
            r = self.player_order[i]
            self.writer[r] = w
            self.reciever[w] = r
        #print(self.players.keys(), random.shuffle(self.players.keys()), self.player_order, n)
        self.writer[self.player_order[0]] = self.player_order[n-1]
        self.reciever[self.player_order[n-1]] = self.player_order[0]
        
        for w,r in self.writer.items():
            await self.channels[w].send("Wie soll **" + r.name + "** heißen? Bitte nur den Namen schreiben.")

        results = await self.wait_for_dm_answers(self.writer.keys())
        if results is None:
            msg = Message("Nicht alle haben geantwortet", 1)
            await msg.submit(channel)
            self.end()
            return
        results = {self.writer[u]: results[u] for u in self.player_order}

        for r in self.reciever:
            others = [u.name + " " + v for u, v in results.items() if u != r]
            await self.channels[r].send(string_list("Namen", others))

        msg = Message("Done", 0)
        await msg.submit(channel)
        self.end()