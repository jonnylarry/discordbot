from games.game import Game, Message
import asyncio
import discord
import random


# 0x1F1E6 = 

class NobodyIsPerfect(Game):
    def __init__(self, code, client, cog):
        super().__init__(code, client, cog)
        self.name = "NobodyIsPerfect"

    def start_conditions(self):
        return 27 > len(self.players) > 0 and super().start_conditions()

    async def start(self, channel):
        msg = await super().start(channel)
        if msg.hasErr():
            return msg
        else:
            msg.submit(channel)
        self.points = None
        self.questioners = iter(self.player_order)

        await self.round(channel)

    async def round(self, channel):
        try:
            questioner = next(self.questioners)
        except StopIteration:
            await channel.send("The end!")
            self.end()

        question = await self.ask_question(questioner, channel)
        if question is None:
            return
        head_question = "Wie würde " + questioner.name + " am ehsten auf diese Frage antworten: \n *" + question.content + "*"
        answers = await self.collect_answers(head_question, channel)

        answerUsers = list(answers.keys())
        random.shuffle(answerUsers)
        msgID, emoji_author_map = await self.present_answers(answers, answerUsers, head_question, channel)

        emojis = emoji_author_map.keys()
        tally = await self.tally_results(msgID, emojis, channel)

        print("RESULTS ARE IN")

        deltapoints = await self.score(emoji_author_map, tally, questioner)
        pp = await self.print_points(deltapoints)
        await channel.send(pp + "\n ?game.continue zum fortsetzen!")
        return        



    ########################################################################
    ###### UTILITY:

    async def ask_question(self, questioner, channel):
        await self.channels[questioner].send("Stell hier eine Frage o.Ä.")
        def check(msg):
            return msg.author == questioner and isinstance(msg.channel, discord.channel.DMChannel)
        try:
            question = await self.client.wait_for("message", check=check, timeout=120) # 120 seconds to reply
        except asyncio.TimeoutError:
            msg = Message(questioner.name + " hat keine Frage gestellt.", 1)
            await msg.submit(channel)
            return None
        return question



    async def collect_answers(self, head_question, channel):
        for u in self.player_order:
            await self.channels[u].send(head_question)
        
        answers = await self.wait_for_dm_answers(self.player_order)
        if answers is None:
            msg = Message("Nicht alle haben geantwortet", 1)
            await msg.submit(channel)
            return None
        return answers



    async def present_answers(self, answers,answerUsers, head_question, channel):
        answer_emojis = []
        message_possible_answers = head_question
        emoji_author_map = dict()
        for i,u in enumerate(answerUsers):
            #answer_emojis.append(emoji.emojize(":regional_indicator_" + chr(ord('a')+i) + ":"))
            answer_emojis.append(chr(0x1F1E6 + i))
            message_possible_answers += "\n" + answer_emojis[i] + " " + answers[u]
            emoji_author_map[answer_emojis[i]] = u

        msg = await channel.send(message_possible_answers)
        for e in answer_emojis:
            print(e)
            await msg.add_reaction(e)

        return msg.id, emoji_author_map


    
    async def tally_results(self, msgID, emojis, channel, max_wait_time=100):
        delay = 10
        i = int(max_wait_time/delay)
        while i > 0:
            i -= 1
            print("WAITING for results")
            await asyncio.sleep(delay)
            total, res = await self.tally(msgID, emojis, channel)
            print("VOTES:", total)
            if total >= len(self.players):
                break
        
        return res



    async def tally(self, msgID, options, channel):
        # from here https://gist.github.com/Vexs/f2c1bfd6bda68a661a71accd300d2adc

        voters = set([self.client.user])  # add the bot's ID to the list of voters to exclude it's votes
        res = {x: set() for x in options}
        total_votes = 0
        poll_message = await channel.fetch_message(msgID)
        for reaction in poll_message.reactions:
            if reaction.emoji in options:
                async for reactor in reaction.users():
                    if reactor not in voters:
                        total_votes += 1
                        res[reaction.emoji].add(reactor)
                        voters.add(reactor)

        return total_votes, res



    async def score(self, emoji_author_map, res, questioner):
        votes = {}
        for e,a in emoji_author_map.items():
            votes[a] = res[e]
            if a in votes[a]:
                votes[a].remove(a)

        result = dict()
        for a in self.player_order:
            i = 0
            if a in votes[questioner]:
                i = 1
            result[a] = len(votes[a]) + i
        return result



    async def print_points(self, delta=None):
        if self.points is not None:
            for u in self.points.keys():
                self.points[u] += delta[u]
        else:
            self.points = delta
        scored_points_msg = "Punkteverteilung:\n - "
        scored_points_msg += "\n - ".join([a.name + ": " + str(p) + " (+" + str(delta[a]) + ") " for a,p in self.points.items()])
        return scored_points_msg