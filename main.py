import os
from unicodedata import category
from dotenv import load_dotenv
from discord.ext import commands
import discord
from instantbtn.instantbtn_bot import InstantBtnBot
from language.language_bot import LanguageBot

from risiko.coronabot import Corona
import logging
import asyncio
from discord.ext import tasks
from datetime import datetime

# https://stackoverflow.com/questions/4563272/how-to-convert-a-utc-datetime-to-a-local-datetime-using-only-standard-library
import pytz
local_tz = pytz.timezone('Europe/Berlin')
def utc_to_local(utc_dt):
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
    return local_tz.normalize(local_dt) # .normalize might be unnecessary
def aslocaltime(utc_dt):
    return utc_to_local(utc_dt)

def set_logger(loggername):
    logger = logging.getLogger(loggername)
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename=loggername + '.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)
    return logger

set_logger('discord')
mainLogger = set_logger('main')

debug = False
server = 542733091425353749
if debug:
    server = 835556115298254853

class MyClient(commands.Bot): #(discord.Client):
    def __init__(self):
        super().__init__('?')

    async def on_ready(self):
        print("logged in")
        mainLogger.debug("logged in")
        #conspiracy.start(self)

    async def on_message(self, message):
        mainLogger.info("Message: " + message.content)
        await self.process_commands(message)
    
#@tasks.loop(minutes=10.0)
#async def conspiracy(self):
#    if aslocaltime(datetime.utcnow()).hour in (16, 21):
#        mainLogger.debug("conspiracy")
#        guild = self.get_guild(server)
#        category = discord.utils.get(guild.categories, name="Brutalekillerspiele")
#        channel = discord.utils.get(category.voice_channels, name="Befreiung")
#        if channel:
#            await channel.delete()
#        channel = await guild.create_voice_channel("Befreiung", category=category, user_limit=1)
#        vc_client = await channel.connect()
#        await asyncio.sleep(60*30)
#        await vc_client.disconnect()
#        await channel.delete()


# testserver id = 835556115298254853

def setup(client):
    load_dotenv()
    #client.add_cog(Music(client))
    client.add_cog(Corona(client))
    #client.add_cog(Games(client))
    client.add_cog(LanguageBot(client))
    client.add_cog(InstantBtnBot(client))
    #client.add_cog(MusicEvents(client))
    client.run(os.getenv('SECRET'))
    #client.run(os.getenv('SECRETDEBUG'))

client = MyClient()         
setup(client)
