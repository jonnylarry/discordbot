import datetime as dt
import pytz

BERLINTZ = pytz.timezone("Europe/Berlin")


timezones = {-12: 'IDLW', -11: 'SST', -10: 'HST', -9: 'AKST', -8: 'AKDT', -7: 'MST', -6: 'MDT', -5: 'CDT', -4: 'EDT', -3: 'ADT', -2: 'BRST', -1: 'EGT', 0: 'UTC', 1: 'WEST', 2: 'CEST', 3: 'EEST', 4: 'GST', 5: 'HMT', 6: 'OMST', 7: 'WIB', 8: 'AWST', 9: 'JST', 10: 'AEST', 11: 'SBT', 12: 'FJT', 13: 'NZDT', 14: 'LINT'}

now = dt.datetime.utcnow().replace(minute=0,second=0,microsecond=0)
print(now)
eleven = dt.datetime.utcnow().replace(hour=23,minute=0,second=0,microsecond=0)

delta = eleven - now
hours_delta = delta.seconds // 3600

if -12 > hours_delta:
    hours_delta += 12
elif hours_delta > 12:
    hours_delta -= 12

print(timezones[hours_delta])