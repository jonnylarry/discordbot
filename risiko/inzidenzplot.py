import pandas as pd
from matplotlib import pyplot as plt
from datetime import datetime


def plot(data, a, b):
    df = pd.DataFrame.from_records(data)#.transpose()
    df.columns = ["count", "Date", "content"]
    print(df)
    #df = df.groupby(pd.Grouper(key='Date',freq='1D')).sum()

    if a and b:
        s,e = datetime.strptime(a, '%Y-%m-%d'), datetime.strptime(b, '%Y-%m-%d')
        mask = (df['Date'] > s) & (df['Date'] <= e)
        df = df.loc[mask]
    
    print(df)

    plot = df.plot(x="Date", y="count", figsize=(15,4))

    df2 = df.set_index('Date').rolling('6H', min_periods=1).sum()
    print(df)
    plot = df2.plot(y="count", figsize=(15,4))

    fig = plot.get_figure()
    fig.savefig("./data/risiko/inzidenz.png")

    return min(df['Date']).strftime("%m/%d/%Y, %H:%M:%S"), max(df['Date']).strftime("%m/%d/%Y, %H:%M:%S")



def example():
    data = [
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "Command not found"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!test3"],
    [2, datetime.strptime("2021-04-17", '%Y-%m-%d'), "?test2"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "Command not found"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!test2"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!test"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [2, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "!"],
    [1, datetime.strptime("2021-04-17", '%Y-%m-%d'), "t"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "nvbnv,hkj"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "d"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "NO"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "NO"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix  bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "NO"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix  bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "NO"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix  bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix  bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "NO"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "fix bot"],
    [1, datetime.strptime("2020-10-29", '%Y-%m-%d'), "bla"],
    [1, datetime.strptime("2020-10-28", '%Y-%m-%d'), "a"],
    [1, datetime.strptime("2020-10-28", '%Y-%m-%d'), "dsad"],
    [1, datetime.strptime("2020-10-28", '%Y-%m-%d'), "😷"],
    [1, datetime.strptime("2020-10-28", '%Y-%m-%d'), "😷"],
    [1, datetime.strptime("2020-10-28", '%Y-%m-%d'), "😷"]
    ]

    start, stop = inzidenzplot.plot(data, ["2021-04-01", "2021-04-18"])
