import discord
import os
from discord.ext import commands
from collections import defaultdict
import asyncio
import logging


from risiko.inzidenzplot import plot as inziplot

logger = logging.getLogger('main')

class Corona(commands.Cog):
    def __init__(self, bot):
        self.client = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return  
        if ":mask:" not in message.content and os.getenv('RISIKOGEBIET') in message.channel.name and "😷" not in message.content:
            print("print corona")
            await message.add_reaction(self.client.emojis[0])

    @commands.command(name='inzidenz')
    async def inzidenz(self, ctx, a=None, b=None):
        """Plots the Inzidenz between date a and b if specified. Date format: 'm/d/Y, H:M:S'"""
        message = ctx.message
        print("hallo ", a, b)
        messages = await message.channel.history(limit=500).flatten()

        corona_datapoints = []
        for m in messages:
            corona_reaction = [r for r in m.reactions if r.emoji == self.client.emojis[0]]
            if corona_reaction:
                corona_reaction = corona_reaction[0]
                corona_datapoints.append([corona_reaction.count, m.created_at, m.content])
        print(corona_datapoints)
        # zb "2021-04-01" - "2021-04-18"
        start, stop = inziplot(corona_datapoints, a, b)
        print("print inzidenz")
        await message.channel.send('coronaemojis between ' + start + ' and ' + stop + ':mask:', file=discord.File('./data/risiko/inzidenz.png') );

    ############################################################################################
    # ACHTUNG inperformant und wird evtl problematisch als API-Spam erkannt
    @commands.command(name='spreader')
    async def spreader(self, ctx):
        """Plots Top Ten Spreaders and Covidiots"""
        message = ctx.message
        messages = await message.channel.history(limit=200).flatten()
        spreader = defaultdict(int)
        covidiots = defaultdict(int)
        for m in messages:
            corona_reaction = [r for r in m.reactions if r.emoji == self.client.emojis[0]]
            if corona_reaction:
                corona_reaction = corona_reaction[0]
                users = await corona_reaction.users().flatten()
                covidiots[m.author.name] += len(users)
                for u in users:
                    spreader[u.name] += 1
        print("spreader")
        toptenSpreader = sorted(list(spreader.items()), key=lambda x: -x[1])[:5]
        toptenCovidiots = sorted(list(covidiots.items()), key=lambda x: -x[1])[:5]

        msg = '**Top Spreader last 200 Messages:**\n'
        for t in toptenSpreader:
            msg += "*" + t[0] + ": " + str(t[1]) + "* \n"
        msg += '**Top Covidiots last 200 Messages:**\n'
        for t in toptenCovidiots:
            msg += "*" + t[0] + ": " + str(t[1]) + "* \n"
        await message.channel.send(msg)


    @commands.command(name='testtts')
    async def testtts(self, ctx):
        await asyncio.sleep(10) 
        await ctx.send("This is a tts message", tts=True)

        

    #async def on_message(self, message):
    #    print(message.content)

