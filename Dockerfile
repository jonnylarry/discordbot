FROM python:3.8.9-slim-buster
WORKDIR /home/discordbot

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y git \
    hunspell \
    hunspell-tools \
    python-enchant \
    libenchant-2-2 \
    ffmpeg \
    nodejs \
    yarn \
    make

COPY requirements.txt /home/discordbot
RUN pip install --no-cache-dir -r /home/discordbot/requirements.txt
RUN python3 -m pip install -U discord.py[voice]

COPY . /home/discordbot
COPY .env /home/discordbot

CMD [ "python", "./main.py" ]