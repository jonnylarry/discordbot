import asyncio
from discord.ext import commands
import discord
import json
import os
import datetime as dt
import pytz

TIMEZONES = {-12: 'IDLW', -11: 'SST', -10: 'HST', -9: 'AKST', -8: 'AKDT', -7: 'MST', -6: 'MDT', -5: 'CDT', -4: 'EDT', -3: 'ADT', -2: 'BRST', -1: 'EGT', 0: 'UTC', 1: 'WEST', 2: 'CEST', 3: 'EEST', 4: 'GST', 5: 'HMT', 6: 'OMST', 7: 'WIB', 8: 'AWST', 9: 'JST', 10: 'AEST', 11: 'SBT', 12: 'FJT', 13: 'NZDT', 14: 'LINT'}

BERLINTZ = pytz.timezone("Europe/Berlin")
DATA_PATH = "./mp3s/"
DATABASE_PATH = f"{DATA_PATH}database.json"

def old_get_new_channel_name():
    h = int(dt.datetime.now(BERLINTZ).strftime("%I"))
    return f"{h}_Uhr_Runde"

def get_new_channel_name():
    now = dt.datetime.utcnow().replace(minute=0,second=0,microsecond=0)
    eleven = dt.datetime.utcnow().replace(hour=23,minute=0,second=0,microsecond=0)

    delta = eleven - now
    hours_delta = delta.seconds // 3600
    if -12 > hours_delta:
        hours_delta += 12
    elif hours_delta > 12:
        hours_delta -= 12

    return f"11({TIMEZONES[hours_delta]})_Uhr_Runde"

top5 = [1,2,3,4,5]
playlists = {"top5": top5}
emojis = ["1️⃣","2️⃣","3️⃣","4️⃣","5️⃣"]
emojiID = {e:i for i,e in enumerate(emojis)}
with open(DATABASE_PATH, "r") as f:
    database = json.load(f)


class InstantBtnBot(commands.Cog):
    def __init__(self, bot):
        self.client = bot

    @commands.command(pass_context=True, name='btn', aliases=['b'])
    async def btn(self, ctx, value):
        """?btn *buttonName*"""
        voicechannel = ctx.message.author.voice.channel
        await self.run_button(value, voicechannel, ctx.message.channel)
    
    @commands.command(pass_context=True, name='btnTo', aliases=['bTo'])
    async def btnTo(self, ctx, voicechannel: discord.VoiceChannel, value):
        """?btnTo *Channel* *buttonName*"""
        await self.run_button(value, voicechannel, ctx.message.channel)
    
    @commands.command(pass_context=True, name='writeBtn', aliases=['writebtn', 'write', 'wb'])
    async def writeBtn(self, ctx, value):
        """e.g. mp3 attachment with command '?writeBtn test' results in button test"""
        value = value.lower()
        path = f"{DATA_PATH}{value}.mp3"
        await ctx.message.attachments[0].save(path)
        database["buttons"][value] = {"path": path, "count": 0}
        self.updateDatabase()
        await send_done(ctx.message.channel)

    @commands.command(pass_context=True, name='deleteBtn', aliases=['delbtn', 'delBtn', 'deletebtn'])
    async def deleteBtn(self, ctx, value):
        """?deleteBtn *buttonName*"""
        value = value.lower()
        if value not in database["buttons"]:
            return
            
        os.remove(database["buttons"][value]["path"]) 
        del database["buttons"][value]
        self.updateDatabase()
        await send_removed(ctx.message.channel)

    @commands.command(pass_context=True, name='listBtn', aliases=['listbtn'])
    async def listBtn(self, ctx):
        """Lists all buttons"""
        await ctx.message.channel.send("Button Liste\n - " + "\n - ".join(database["buttons"].keys()))

    @commands.command(pass_context=True, name='playlist')
    async def playlist(self, ctx, value):
        """Shows Button Quickselect: ?playlist *playlistName*"""
        if value not in database["playlists"]:
            await send_notfound(ctx.message.channel)
            return
        if value == "top5":
            self.updateTop5()

        text = f"Button Playlist: {value}\n"
        for i,v in enumerate(database["playlists"][value]):
            text += f"{emojis[i]} {v} \n"
        msg = await ctx.message.channel.send(text)
        for i,v in enumerate(database["playlists"][value]):
            text += f"{emojis[i]} {v} \n"
            await msg.add_reaction(emojis[i])

    @commands.command(pass_context=True, name='writeplaylist')
    async def writeplaylist(self, ctx, name, *values):
        """Writes a playlist ?writeplaylist *playlistName*"""
        database["playlists"][name] = values
        self.updateDatabase()
        await send_done(ctx.message.channel)


    @commands.command(pass_context=True, name='listplaylist')
    async def listplaylist(self, ctx):
        """Shows all playlists"""
        await ctx.message.channel.send("Playlists\n - " + "\n - ".join(database["playlists"].keys()))


    # Listeners
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        not_before = before.channel is None or not "Uhr_Runde" in before.channel.name
        not_after = after.channel is None or not "Uhr_Runde" in after.channel.name

        if not_before and not not_after and after.channel.name != get_new_channel_name():
            await after.channel.edit(name=get_new_channel_name())
        if not not_before and not_after and (len(before.channel.members) < 1 and before.channel.name != "11_Uhr_Runde"):
            await before.channel.edit(name="11_Uhr_Runde")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        channel = await self.client.fetch_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        emoji = str(payload.emoji)
        if not message.content.startswith("Button Playlist: "):
            return
        if payload.member.bot:
            return
        await message.remove_reaction(emoji, payload.member)
        await self.play_from_playlist(message, payload.member.voice.channel, emoji)


    # Brain
    async def play_from_playlist(self, message, voicechannel, emoji):
        emoji = str(emoji)
        playlist = message.content.split("\n")[0][len("Button Playlist: "):]
        playlist = playlist.lower()
        if not playlist in database["playlists"]:
            return
        if not emoji in emojiID:
            return
        if not playlist in database["playlists"]:
            return
        
        await self.run_button(database["playlists"][playlist][emojiID[emoji]], voicechannel, message.channel)


    async def run_button(self, value, voicechannel, channel):
        if not voicechannel:
            return
        value = value.lower()

        vc = await voicechannel.connect()
        if value not in database["buttons"]:
            await send_notfound(channel)
            await vc.disconnect()
            return
        vc.play(discord.FFmpegPCMAudio(database["buttons"][value]["path"]), after=lambda e: print('done', e))
        while vc.is_playing():
            await asyncio.sleep(1)
        database["buttons"][value]["count"] += 1
        await vc.disconnect()

    def updateTop5(self):
        database["playlists"]["top5"] = [i for i,j in sorted(database["buttons"].items(), key=lambda a: a[1]["count"], reverse=True)[:5]]
        self.updateDatabase()

    def updateDatabase(self):
        with open(DATABASE_PATH, 'w') as f:
            json.dump(database, f)


async def send_done(channel):
    await channel.send("done")

async def send_notfound(channel):
    await channel.send("not in database")

async def send_removed(channel):
    await channel.send("removed")