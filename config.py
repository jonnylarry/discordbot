import json
config = {}

def __assoc_in(dct, path, value):
    for x in path:
        prev, dct = dct, dct.setdefault(x, {})
    prev[x] = value

def __remove_in(dct, path, value):
    for x in path:
        prev, dct = dct, dct.setdefault(x, {})
    prev[x].pop(value)

def __get_in(dct, path):
    for x in path:
        prev, dct = dct, dct[x]
    return prev[x]

def import_config():
    global config
    with open('config.json', 'r') as f:
        config = json.load(f)

def export_config():
    with open('config.json', 'w') as f:
        json.dump(config, f, indent=4, sort_keys=True)

def write_config(path_to_key, val):
    __assoc_in(config, path_to_key, val)
    export_config()

def remove(path):
    __remove_in(config, path[:-1], path[-1])
    export_config()

def reset_config():
    global config
    config = {
            "ENABLE_ANNOUNCEMENT": True, 
            "SANDMANN_TIME": "3",
            "ANNOUNCEMENTS": {
                "jonny_larry": {
                    "msg": "Ich verkünde der hochedele ehrsame %s ist beigetreten ihr Plebs.",
                    "lang": "DE",
                    "slow": False
                }
            }
        }
    export_config()
#reset_config()
import_config()
